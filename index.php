<!doctype html>
<html lang="es">
<?php
include "config/Conexion.php";
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/style_login.css">
    <link rel="icon" type="image/png" href="assets/img/<?php echo($db_alias) ?>/favicon.png">
    <title>Sistema de Ventas</title>
</head>

<body style="background-image: url('assets/img/<?php echo $db_alias?>/background_login.jpg')">

<div class="row">
    <div class="col-md-12">
        <div class="conta">
            <form action="/<?php echo($project) ?>" method="post">
                <hr>
                <h2 class="text-center"><?php echo($db_empresa)?></h2>
                <div class="form-group">
                    <i class="fa fa-user"></i>
                    <label for="username">Username</label>
                    <input class="text-muted form-control" type="text" name="username" required>
                </div>
                <div class="form-group">
                    <i class="fa fa-unlock-alt"></i>
                    <label for="password">Password</label>
                    <input class="text-muted form-control" type="password" name="username" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block">Ingresar al Sistema</button>
                </div>
            </form>
        </div>
    </div>

</div>
</body>
</html>